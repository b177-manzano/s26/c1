//Get max price in origin philippines
db.fruits.aggregate([
   { $match: { origin: "Philippines" } },
   { $group: { _id: "Origin: Philippines", max_price: { $max: "$price" } } }
]);

