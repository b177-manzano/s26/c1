//Create fruits collection
db.fruits.insertMany([
    {
        name: "Apple",
        color: "Red",
        stock: 20,
        price: 40,
        supplier_id: 1,
        onSale: true,
        origin: ["Philippines", "US"]
    },
    {
        name: "Banana",
        color: "Yellow",
        stock: 15,
        price: 20,
        supplier_id: 2,
        onSale: true,
        origin: ["Philippines", "Ecuador"]
    },
    {
        name: "Kiwi",
        color: "Green",
        stock: 25,
        price: 50,
        supplier_id: 1,
        onSale: true,
        origin: ["US", "China"]
    },
    {
        name: "Mango",
        color: "Yellow",
        stock: 10,
        price: 120,
        supplier_id: 2,
        onSale: false,
        origin: ["Philippines", "India"]
    }
]);

// ================================
//  27 May 2022 Activity

// Average of stocks of fruits on sale
// Avarege of stocks of fruits based on specific suppliers

db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "_id", AveragePrice: { $avg: "$price" } } }
]);

db.fruits.aggregate([
   { $match: { onSale: true } },
   { $group: { _id: "$supplier_id", avg_price: { $avg: "$price" } } }
]);



